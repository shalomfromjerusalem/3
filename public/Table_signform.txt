CREATE TABLE signform (
id int(10) unsigned NOT NULL AUTO_INCREMENT,
name varchar(128) NOT NULL DEFAULT ' ',
email varchar(30) NOT NULL DEFAULT ' ',
birthday date NOT NULL,
sex varchar(10) NOT NULL DEFAULT ' ',
limb int(10) NOT NULL,
superpowers varchar(128) NOT NULL DEFAULT ' ',
bio text NOT NULL DEFAULT ' ',
PRIMARY KEY (id),
UNIQUE (email)
);