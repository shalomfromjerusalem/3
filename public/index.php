<!DOCTYPE html>
<html lang="ru">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SP25</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- My CSS -->
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <!-- DESCRIPTION -->
    <div class="col-lg-5 mx-lg-auto mt-5">


        <div class="describe">
            <h1>Задание №3</h1>
            <p id="00"> Реализуйте скрипт на веб-сервере на PHP или другом языке программирования, сохраняющий в базу данных MySQL (MariaDB) заполненную форму.</p>
            <div class="text-center"> <img src="img/2020-03-25_235648.jpg" alt="image00" class="mx-auto img-fluid">
            </div>
        </div>
        
        <div class="form">
            <fieldset class="scheduler-border-order">
                <legend class="scheduler-border">HTML-Форма </legend>
                <form action="form.php" method="POST">
                    <div class="col">
                        <div class="form-group">
                            <label for="name">Имя:</label>
                            <input type="text" class="form-control" name="name" placeholder="Ваше имя">
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" name="email" placeholder="your@email.com">
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <label for="birthday">Дата рождения:</label>
                            <input type="date" class="form-control" name="birthday">
                        </div>
                    </div>

                    <div class="col">
                        <div id="radio-div1" class="form-group">
                            <label for="sex">Пол:</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sex" id="sex1"
                                    value="Мужской" checked>
                                <label class="form-check-label" for="sex1">Мужской</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sex" id="sex2"
                                    value="Женский">
                                <label class="form-check-label" for="sex2">Женский</label>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div id="radio-div2" class="form-group">
                            <label for="limb">Количество конечностей:</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb1"
                                    value="1" checked>
                                <label class="form-check-label" for="limb1">1</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb2"
                                    value="2">
                                <label class="form-check-label" for="limb2">2</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb3"
                                    value="3">
                                <label class="form-check-label" for="limb3">3</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb4"
                                    value="4">
                                <label class="form-check-label" for="limb4">4</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="superpowers">Сверхспособности(множественный выбор из списка):</label>
                        <select multiple class="form-control" name="Superpowers[]">
                            <option value="Бессмертие">Бессмертие</option>
                            <option value="Прохождение сквозь стены">Прохождение сквозь стены</option>
                            <option value="Левитация">Левитация</option>
                            <option value="Телекинез">Телекинез</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="bio">Биография:</label>
                        <textarea class="form-control" name="bio" rows="5" placeholder="О себе"></textarea>
                    </div>

                    <div class="form-check">
                        <input type="checkbox" name="checkbox" class="form-check-input" id="Check1">
                        <label class="form-check-label" for="Check1">С контрактом ознакомлен(а)</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Отправить</button>

                </form>
            </fieldset>
        </div>
    </div>
    </div>
    </body>
</html>