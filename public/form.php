<?php
    header('Content-Type: text/html; charset=UTF-8');    
    
    if($_SERVER['QUERY_STRING'] == 'success') {
        print('Ваши данные отправлены!');
        
        exit();
    }

    $errors = getArrayErrors();
    
    if (count($errors) > 0) {
        printErrors($errors);
        
        exit();
    }  
    
    add();
    
    header('Location: ?success');
    
    function getArrayErrors() {
        $errors = array();
        
        if(empty($_POST['name'])) {
            $errors[] = 'Поле "Имя" пустое';
        } else if (strlen($_POST['name']) > 20) {
            $errors[] = 'Поле "Имя" слишком длинное';
        }
        
        if(empty($_POST['email'])) {
            $errors[] = 'Поле "Email" пустое';
        } else if (strlen($_POST['email']) > 320) {
            $errors[] = 'Поле "Email" слишком длинное';
        } else if ( !strpos($_POST['email'], '@') || 0 == strpos($_POST['email'], '@')
            || strlen($_POST['email'])-1 == strpos($_POST['email'], '@') ) {
            $errors[] = 'Не верный формат ввода в поле "Email"';
        }
        
        if(empty($_POST['bio'])) {
            $errors[] = 'Поле "Биография" пустое';
        } else if (strlen($_POST['bio']) > 320) {
            $errors[] = 'Поле "Биография" слишком длинное';
        }
        
        if(empty($_POST['checkbox'])) {
            $errors[] = 'Условия не были приняты';
        }
        
        return $errors;
    }
    
    function printErrors($errors) {
        $strErrors = $errors[0];
        
        for ($i = 1; $i < count($errors); $i++) {
            $strErrors .= '<br>' . $errors[$i];
        }
        
        print($strErrors);
    }
    
    function add() {
        $connection = 'mysql:host=localhost;dbname=u17333';
        $pdo = new PDO($connection, 'u17333', '7038049');
        
        $sql = 'INSERT INTO signform SET name = ?, email = ?, birthday = ?, sex = ?, limb = ?,
                superpowers = ?, bio = ?';
        
        $stmt = $pdo->prepare($sql);
        
        $stmt->execute(array($_POST['name'], $_POST['email'], $_POST['birthday'], $_POST['sex'],
            $_POST['limb'], serialize($_POST['Superpowers']), $_POST['bio']));
    }
?>